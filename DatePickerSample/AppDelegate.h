//
//  AppDelegate.h
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic)IBOutlet UIWindow *window;


@end

