//
//  ViewController.h
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "kCustomDatePicker.h"

@interface ViewController : UIViewController <kCustomDatePickerDelegate>

@property (nonatomic,strong)kCustomDatePicker *datePicker;

- (IBAction)onClickOfOpenDatePickerButton:(UIButton *)sender;

@end
