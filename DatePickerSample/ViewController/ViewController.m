//
//  ViewController.m
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickOfOpenDatePickerButton:(UIButton *)sender
{
    kCustomDatePicker *customDatePicker = [[kCustomDatePicker alloc] initWithDelegate:self andPickerStyle:UIDatePickerModeDate];
    [customDatePicker showDatePickerAnimation:YES onView:self.view];
}

#pragma mark - kdatePicker Delegate Methods -
-(void)datePicker:(kCustomDatePicker *)picker onClickOfCancelButton:(UIBarButtonItem *)button
{
    //Delegate Method Returning Callback for pressing of Cancel Button on CustomDatePicker View
}

-(void)datePicker:(kCustomDatePicker *)picker onClickOfDoneButton:(UIBarButtonItem *)button selectedDate:(NSDate *)selectedDate
{
    //Delegate Method Returning Callback for pressing of Done Button on CustomDatePicker View along with selected Date
}



@end
