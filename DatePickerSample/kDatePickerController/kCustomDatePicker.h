//
//  kCustomDatePicker.h
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class kCustomDatePicker;

@protocol kCustomDatePickerDelegate <NSObject>

@optional
-(void)datePicker:(kCustomDatePicker *)picker onClickOfDoneButton:(UIBarButtonItem *)button selectedDate:(NSDate *)selectedDate;
-(void)datePicker:(kCustomDatePicker *)picker onClickOfCancelButton:(UIBarButtonItem *)button;
@end

@interface kCustomDatePicker : UIView

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property(nonatomic,strong)NSDate *selectedDate;

@property(nonatomic,assign)id<kCustomDatePickerDelegate> delegate;

- (IBAction)onClickOfCancelPickerButton:(UIBarButtonItem *)sender;
- (IBAction)onClickOfDonePickerButton:(UIBarButtonItem *)sender;

-(instancetype)initWithDelegate:(id)delegate andPickerStyle:(UIDatePickerMode)datePickerMode;

-(void)showDatePickerAnimation:(BOOL)animation onView:(UIView *)superView;


@end
