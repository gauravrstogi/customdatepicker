//
//  kCustomDatePicker.m
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import "kCustomDatePicker.h"
#import "AppDelegate.h"

#define kAnimationTimeDuration 0.5

@interface kCustomDatePicker ()
@property(nonatomic,strong)AppDelegate *appDelegate;

@end

@implementation kCustomDatePicker

-(instancetype)initWithDelegate:(id)delegate andPickerStyle:(UIDatePickerMode)datePickerMode
{
    self = [super init];
    if (self) {
        self = (kCustomDatePicker *)[[[NSBundle mainBundle] loadNibNamed:@"kCustomDatePicker" owner:self options:nil] firstObject];
        [self.datePicker setDatePickerMode:datePickerMode];
        self.delegate = delegate;
        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        CGRect frame = self.appDelegate.window.frame;
        [self setFrame:CGRectMake(0, frame.size.height, frame.size.width,206.0f)];
    }
    return self;
}

-(void)showDatePickerAnimation:(BOOL)animation onView:(UIView *)superView
{
    if (animation) {
        //Adding DatePicker View to the superview with Animation
        [UIView animateWithDuration:kAnimationTimeDuration animations:^{
            [superView addSubview:self];
            CGRect frame = self.frame;
            frame.origin.y=frame.origin.y-206.0f;
            [self setFrame:frame];
        }];
    }
    else{
        //Adding DatePicker View to the superview without Animation
    }
}

-(IBAction)onClickOfCancelPickerButton:(UIBarButtonItem *)sender {
    
    [UIView animateWithDuration:kAnimationTimeDuration animations:^{
        CGRect frame = self.frame;
        frame.origin.y+=self.frame.size.height;
        [self setFrame:frame];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (self.delegate && [self.delegate respondsToSelector:@selector(datePicker:onClickOfCancelButton:)]) {
            [self.delegate datePicker:self onClickOfCancelButton:sender];
        }
    }];
    
}

- (IBAction)onClickOfDonePickerButton:(UIBarButtonItem *)sender {
    
    [UIView animateWithDuration:kAnimationTimeDuration animations:^{
        CGRect frame = self.frame;
        frame.origin.y+=self.frame.size.height;
        [self setFrame:frame];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (self.delegate && [self.delegate respondsToSelector:@selector(datePicker:onClickOfDoneButton:selectedDate:)]) {
            [self.delegate datePicker:self onClickOfDoneButton:sender selectedDate:self.datePicker.date];
        }
    }];
}

@end
