//
//  main.m
//  DatePickerSample
//
//  Created by Gaurav Rastogi on 22/11/15.
//  Copyright (c) 2015 Global Logic India Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
